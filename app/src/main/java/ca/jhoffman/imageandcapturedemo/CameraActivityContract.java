package ca.jhoffman.imageandcapturedemo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class CameraActivityContract extends ActivityResultContract<Void, Bitmap> {
    @NonNull
    @Override
    public Intent createIntent(@NonNull Context context, Void input) {
        return new Intent(context, CameraActivity.class);
    }

    @Override
    public Bitmap parseResult(int resultCode, @Nullable Intent intent) {
        return (Bitmap) intent.getParcelableExtra("KEY_THUMBNAIL");
    }
}
