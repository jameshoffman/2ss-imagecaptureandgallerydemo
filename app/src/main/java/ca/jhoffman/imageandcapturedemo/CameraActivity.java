package ca.jhoffman.imageandcapturedemo;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.common.util.concurrent.ListenableFuture;

import java.util.concurrent.ExecutionException;

public class CameraActivity extends AppCompatActivity {

    private PreviewView previewView;
    private FloatingActionButton fab;

    private ImageCapture capture = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        previewView = findViewById(R.id.previewView);
        fab = findViewById(R.id.fabCapture);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (capture != null) {
                    fab.setEnabled(false);

                    capture.takePicture(ContextCompat.getMainExecutor(CameraActivity.this), onImageCaptured);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        String cameraPermission = Manifest.permission.CAMERA;

        if (ContextCompat.checkSelfPermission(CameraActivity.this, cameraPermission) == PackageManager.PERMISSION_GRANTED) {
            initPreview();
        } else {
            activityCameraPermissionLauncher.launch(cameraPermission);
        }
    }

    ActivityResultLauncher<String> activityCameraPermissionLauncher = registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            new ActivityResultCallback<Boolean>() {
                @Override
                public void onActivityResult(Boolean granted) {
                    if (granted) {
                        initPreview();
                    } else {
                        Toast.makeText(CameraActivity.this, "Camera permission denied...", Toast.LENGTH_LONG).show();
                    }
                }
            }
    );

    private void initPreview() {
        ListenableFuture<ProcessCameraProvider> providerFuture = ProcessCameraProvider.getInstance(this);

        providerFuture.addListener(new Runnable() {
            @Override
            public void run() {
                try {
                    ProcessCameraProvider provider = providerFuture.get();
                    Preview preview = new Preview.Builder().build();
                    CameraSelector cameraSelector = new CameraSelector.Builder()
                            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                            .build();

                    capture = new ImageCapture.Builder().build();

                    preview.setSurfaceProvider(previewView.getSurfaceProvider());

                    provider.unbindAll();
                    provider.bindToLifecycle((LifecycleOwner) CameraActivity.this, cameraSelector, preview, capture);
                } catch (Exception e) {
                    Toast.makeText(CameraActivity.this, "Could not initialize camera!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }, ContextCompat.getMainExecutor(this));
    }

    private ImageCapture.OnImageCapturedCallback onImageCaptured = new ImageCapture.OnImageCapturedCallback() {
        @Override
        public void onCaptureSuccess(@NonNull ImageProxy image) {
            Bitmap thumbnail = ThumbnailUtils.extractThumbnail(previewView.getBitmap(), 100, 100);

            Intent resultIntent = new Intent();
            resultIntent.putExtra("KEY_THUMBNAIL", thumbnail);
            setResult(Activity.RESULT_OK, resultIntent);

            finish();
        }

        @Override
        public void onError(@NonNull ImageCaptureException exception) {
            Toast.makeText(CameraActivity.this, "Could not capture image", Toast.LENGTH_LONG).show();

            fab.setEnabled(true);
        }
    };
}