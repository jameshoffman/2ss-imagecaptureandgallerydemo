package ca.jhoffman.imageandcapturedemo;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.util.Size;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView)findViewById(R.id.imageView);

        findViewById(R.id.buttonCamera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cameraPermission = Manifest.permission.CAMERA;

                if (ContextCompat.checkSelfPermission(MainActivity.this, cameraPermission) == PackageManager.PERMISSION_GRANTED) {
                    openCamera();
                } else {
                    activityCameraPermissionLauncher.launch(cameraPermission);
                }
            }
        });

        findViewById(R.id.buttonGallery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    activityGalleryLauncher.launch("image/jpg");
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(MainActivity.this, "Could not open gallery!", Toast.LENGTH_LONG).show();
                }
            }
        });

        findViewById(R.id.buttonCustomCamera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityCustomCameraLauncher.launch(null);
            }
        });
    }


    // https://developer.android.com/training/permissions/requesting#request-permission

    ActivityResultLauncher<String> activityCameraPermissionLauncher = registerForActivityResult(
        new ActivityResultContracts.RequestPermission(),
        new ActivityResultCallback<Boolean>() {
            @Override
            public void onActivityResult(Boolean granted) {
                if (granted) {
                    openCamera();
                } else {
                    Toast.makeText(MainActivity.this, "Camera permission denied...", Toast.LENGTH_LONG).show();
                }
            }
        }
    );


    // https://developer.android.com/training/basics/intents/result#register
    // Built-in contracts: https://developer.android.com/reference/androidx/activity/result/contract/ActivityResultContracts

    ActivityResultLauncher<Void> activityCameraLauncher = registerForActivityResult(
            new ActivityResultContracts.TakePicturePreview(),
            new ActivityResultCallback<Bitmap>() {
                @Override
                public void onActivityResult(Bitmap result) {
                    onImageSelected(result);
                }
            }
    );

    ActivityResultLauncher < String > activityGalleryLauncher = registerForActivityResult(
            new ActivityResultContracts.GetContent(),
            new ActivityResultCallback<Uri>() {
                @Override
                public void onActivityResult(Uri result) {
                    Bitmap image = null;

                    try {
                        Size maxSize = new Size(100, 100);
                        image = getContentResolver().loadThumbnail(result, maxSize, null);
                    } catch (Exception e) {
                        Log.d("DEMO", "Could not load image");
                    }

                    onImageSelected(image);
                }
            }
    );

    ActivityResultLauncher<Void> activityCustomCameraLauncher = registerForActivityResult(
            new CameraActivityContract(),
            new ActivityResultCallback<Bitmap>() {
                @Override
                public void onActivityResult(Bitmap result) {
                    onImageSelected(result);
                }
            }
    );

    private void openCamera() {
        try {
            activityCameraLauncher.launch(null);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(MainActivity.this, "Could not find camera!", Toast.LENGTH_LONG).show();
        }
    }

    private void onImageSelected(Bitmap image) {
        if (image != null) {
            String base64 = encodeBase64(image);
            Log.d("DEMO", "onActivityResult: " + base64);

            Bitmap decodedImage = decodeBase64(base64);

            imageView.setImageBitmap(decodedImage);
        } else {
            Toast.makeText(MainActivity.this, "No image selected!", Toast.LENGTH_LONG).show();
        }
    }


    // https://www.thecrazyprogrammer.com/2016/10/android-convert-image-base64-string-base64-string-image.html

    private String encodeBase64(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();

        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    private Bitmap decodeBase64(String base64) {
        byte[] imageBytes = Base64.decode(base64, Base64.DEFAULT);

        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
    }
}